package com.idemon.commons.util;

import java.util.Arrays;

import org.apache.commons.codec.language.Caverphone1;
import org.apache.commons.codec.language.Caverphone2;
import org.apache.commons.codec.language.ColognePhonetic;
import org.apache.commons.codec.language.DoubleMetaphone;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.codec.language.RefinedSoundex;
import org.apache.commons.codec.language.Soundex;

/**
 * 
 * <p>
 * Title: App
 * </p>
 * <p>
 * Description: Metaphone 建立出相同的key给发音相似的单字, 比 Soundex 还要准确, 但是 Metaphone
 * 没有固定长度, Soundex 则是固定第一个英文字加上3个数字. 这通常是用在类似音比对, 也可以用在 MP3 的软件开发.
 * </p>
 * 
 * @author idemon
 * @date 2016年8月25日 上午10:50:43
 */
public class App {
	public static void main(String[] args) {
//		testCodecKit();
//		language();
		System.out.println(CodecKit.encryptByMD5("kexueruanjian"));
		//String[] arr = {"ccc","aaa","bbb","aaa"};
		//System.out.println(Arrays.toString(Kit.deduplicate(arr)));
//		System.out.println(CodecKit.encryptByDES("jdbc:mysql://47.95.22.209/db_soft_down?useUnicode=true&characterEncoding=utf8&autoReconnect=true&failOverReadOnly=false&zeroDateTimeBehavior=convertToNull", "download-sta","UTF-8"));
//		System.out.println(CodecKit.encryptByDES("root", "download-sta","UTF-8"));
//		System.out.println(CodecKit.encryptByDES("pass@mysql", "download-sta","UTF-8"));
	}

	private static void testCodecKit() {
		String s = CodecKit.encryptByDES("Hello World!", "idemon", "tom", "abc");
		System.out.println(s);
		System.out.println(s.length());
		System.out.println(CodecKit.decryptByDES(s, "tom", "idemon", "abc"));

		String t = "Hello";
		System.out.println(CodecKit.encryptByMD5(t));
		System.out.println(CodecKit.encryptByMD5(t).length());
		System.out.println(CodecKit.isEqualsByMD5(t, CodecKit.encryptByMD5(t)));
		System.out.println(CodecKit.encryptBySHA(t));
		System.out.println(CodecKit.encryptBySHA(t).length());
		System.out.println(CodecKit.isEqualsBySHA(t, CodecKit.encryptBySHA(t)));
	}

	private static void language() {
		Metaphone metaphone = new Metaphone();
		RefinedSoundex refinedSoundex = new RefinedSoundex();
		Soundex soundex = new Soundex();
		Caverphone1 caverphone1 = new Caverphone1();
		Caverphone2 caverphone2 = new Caverphone2();
		ColognePhonetic colognePhonetic = new ColognePhonetic();
		DoubleMetaphone doubleMetaphone = new DoubleMetaphone();

		for (int i = 0; i < 2; i++) {
			String str = (i == 0) ? "by" : "buy";

			String mString = null;
			String rString = null;
			String sString = null;
			String cString1 = null;
			String cString2 = null;
			String cpString = null;
			String dmString = null;

			try {
				mString = metaphone.encode(str);
				rString = refinedSoundex.encode(str);
				sString = soundex.encode(str);
				cString1 = caverphone1.encode(str);
				cString2 = caverphone2.encode(str);
				cpString = colognePhonetic.encode(str);
				dmString = doubleMetaphone.encode(str);
			} catch (Exception ex) {
				;
			}
			System.out.println("Original:" + str);
			System.out.println("Metaphone:" + mString);
			System.out.println("RefinedSoundex:" + rString);
			System.out.println("Soundex:" + sString + "\\n");
			System.out.println("Caverphone1:" + cString1 + "\\n");
			System.out.println("Caverphone2:" + cString2 + "\\n");
			System.out.println("ColognePhonetic:" + cpString + "\\n");
			System.out.println("ColognePhonetic:" + colognePhonetic.isEncodeEqual("by", "buy"));
			System.out.println("DoubleMetaphone:" + dmString + "\\n");
			System.out.println("DoubleMetaphone:" + doubleMetaphone.isDoubleMetaphoneEqual("by", "buy"));

		}
	}
}
