package com.idemon.commons.util;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
/**
 * 
 * <p>Title: CodecKit</p>
 * <p>Description: 加密解密</p>
 * 
 * @author idemon
 * @date 2016年8月30日 下午3:56:06
 */
public class CodecKit {

	private static final String DES = "DES";
	private static final String UTF8 = "UTF-8";

	private static Cipher cipher;

	static {
		try {
			cipher = Cipher.getInstance(DES);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 解密(区分大小写)
	 * 
	 * @param ciphertext
	 *            32位密文(区分大小写)
	 * @param keys
	 *            密钥
	 * @return 明文
	 */
	public static synchronized String decryptByDES(String ciphertext, String... keys) {
		try {
			return new String(
					Base64.decodeBase64(des(Cipher.DECRYPT_MODE, generateKey(keys), Base64.decodeBase64(ciphertext))),
					UTF8);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 加密(区分大小写)
	 * 
	 * @param plaintext
	 *            明文
	 * @param keys
	 *            密钥
	 * @return 32位密文(区分大小写)
	 */
	public static synchronized String encryptByDES(String plaintext, String... keys) {
		try {
			return Base64.encodeBase64String(
					des(Cipher.ENCRYPT_MODE, generateKey(keys), Base64.encodeBase64(plaintext.getBytes(UTF8))));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * MD5加密
	 * 
	 * @param plaintext
	 *            明文
	 * @return 大写32位密文
	 */
	public static String encryptByMD5(String plaintext) {
		try {
			return DigestUtils.md5Hex(Base64.encodeBase64(plaintext.getBytes(UTF8))).toUpperCase();
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 判断明文密文是否一致
	 * 
	 * @param plaintext
	 *            明文
	 * @param ciphertext
	 *            密文
	 * @return
	 */
	public static boolean isEqualsByMD5(String plaintext, String ciphertext) {
		return encryptByMD5(plaintext).equals(ciphertext.toUpperCase());
	}

	/**
	 * SHA加密
	 * 
	 * @param plaintext
	 *            明文
	 * @return 大写40位密文
	 */
	public static String encryptBySHA(String plaintext) {
		try {
			return DigestUtils.sha1Hex(Base64.encodeBase64(plaintext.getBytes(UTF8))).toUpperCase();
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 判断明文密文是否一致
	 * 
	 * @param plaintext
	 *            明文
	 * @param ciphertext
	 *            密文
	 * @return
	 */
	public static boolean isEqualsBySHA(String plaintext, String ciphertext) {
		return encryptBySHA(plaintext).equals(ciphertext.toUpperCase());
	}

	private static byte[] des(int opmode, Key key, byte[] data) throws GeneralSecurityException {
		cipher.init(opmode, key);
		return cipher.doFinal(data);
	}

	private static Key generateKey(String... keys) throws GeneralSecurityException {
		Arrays.sort(keys);
		StringBuilder buffer = new StringBuilder(0);
		for (String tmp : keys) {
			buffer.append(tmp);
		}
		return SecretKeyFactory.getInstance(DES).generateSecret(new DESKeySpec(DigestUtils.md5(buffer.toString())));
	}
}
