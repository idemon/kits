package com.idemon.commons.util;
/**
 * 
 * <p>Title: Constants</p>
 * <p>Description: 通用常量</p>
 * 
 * @author idemon
 * @date 2016年8月25日 上午10:49:53
 */
public interface Constants {

	String ENCODING_UTF8 = "UTF-8";
	String ENCODING_GBK = "GBK";
	String ENCODING_ISO = "ISO-8859-1";
	String ENCODING_GB2312 = "GB2312";
	String IDEMON_CONFIG_FILENAME = "idemon-config.properties";
	boolean IS_DEBUG = true;
}
