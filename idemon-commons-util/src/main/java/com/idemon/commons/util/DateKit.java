package com.idemon.commons.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * <p>
 * Title: DateKit
 * </p>
 * <p>
 * Description: 此类提供对日期和日期字符串的操作
 * </p>
 * 
 * @author idemon
 * @date 2016年8月25日 上午10:49:16
 */
public class DateKit {
	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static final String NYRSFM = "yyyy-MM-dd HH:mm:ss";
	/**
	 * yyyy-MM-dd HH:mm:ss SSS
	 */
	public static final String NYRSFMH1 = "yyyy-MM-dd HH:mm:ss SSS";
	/**
	 * yyyyMMddHHmmssSSS
	 */
	public static final String NYRSFMH2 = "yyyyMMddHHmmssSSS";
	/**
	 * yyyyMMddSSS
	 */
	public static final String NYRH = "yyyyMMddSSS";
	/**
	 * yyyy-MM-dd
	 */
	public static final String NYR1 = "yyyy-MM-dd";
	/**
	 * yyyy/MM/dd
	 */
	public static final String NYR2 = "yyyy/MM/dd";
	/**
	 * yyyy.MM.dd
	 */
	public static final String NYR3 = "yyyy.MM.dd";
	/**
	 * yyyyMMdd
	 */
	public static final String NYR4 = "yyyyMMdd";
	/**
	 * yyyy年MM月dd日
	 */
	public static final String NYR5 = "yyyy年MM月dd日";
	/**
	 * HH:mm:ss
	 */
	public static final String SFM1 = "HH:mm:ss";
	/**
	 * HH时mm分ss秒
	 */
	public static final String SFM2 = "HH时mm分ss秒";
	/**
	 * 默认本地格式的SimpleDateFormat对象
	 */
	private static final SimpleDateFormat SDF = new SimpleDateFormat();

	/**
	 * 获取当前毫秒数(从1970开始)
	 * 
	 * @return
	 */
	public static synchronized long getCurrMS() {
		return System.currentTimeMillis();
	}

	/**
	 * 获取当前日期是本年第几周
	 * 
	 * @return
	 */
	public static int getNowWeekOfYear() {
		return Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 获取当前日期是本月第几周
	 * 
	 * @return
	 */
	public static int getNowWeekOfMonth() {
		return Calendar.getInstance().get(Calendar.WEEK_OF_MONTH);
	}

	/**
	 * 获取当前日期是本年第几天
	 * 
	 * @return
	 */
	public static int getNowDayOfYear() {
		return Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
	}

	/**
	 * 获取当前毫秒钟
	 * 
	 * @return
	 */
	public static int getNowMilliSecond() {
		return Calendar.getInstance().get(Calendar.MILLISECOND);
	}

	/**
	 * 获取当前秒钟
	 * 
	 * @return
	 */
	public static int getNowSecond() {
		return Calendar.getInstance().get(Calendar.SECOND);
	}

	/**
	 * 获取当前分钟
	 * 
	 * @return
	 */
	public static int getNowMinute() {
		return Calendar.getInstance().get(Calendar.MINUTE);
	}

	/**
	 * 获取当前小时（24小时制）
	 * 
	 * @return
	 */
	public static int getNowHour() {
		return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 返回当前星期数(星期日是：0；星期一：1)
	 * 
	 * @return
	 */
	public static int getNowWeekDay() {
		return Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
	}

	/**
	 * 返回当前日期是本月第几天（即：号数）（从1开始）
	 * 
	 * @return
	 */
	public static int getNowDay() {
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 获取当前月份（一月份：1；二月份：2）
	 * 
	 * @return
	 */
	public static int getNowMonth() {
		return Calendar.getInstance().get(Calendar.MONTH) + 1;
	}

	/**
	 * 获取当前年份
	 * 
	 * @return
	 */
	public static int getNowYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}

	/**
	 * 获取指定格式的当前日期字符串
	 * 
	 * @return
	 */
	public static String formatNow(String pattern) {
		return format(new Date(), pattern);
	}

	/**
	 * 将指定日期转为指定格式的日期字符串
	 * 
	 * @param date
	 *            日期
	 * @param pattern
	 *            日期格式串
	 * @return 日期字符串
	 */
	public static String format(Date date, String pattern) {
		synchronized (SDF) {
			SDF.applyPattern(pattern);
			return SDF.format(date);
		}
	}

	/**
	 * 将指定日期字符串转为指定格式的日期
	 * 
	 * @param dateStr
	 *            日期字符串
	 * @param pattern
	 *            日期格式串
	 * @return 日期
	 */
	public static Date parse(String dateStr, String pattern) {
		synchronized (SDF) {
			SDF.applyPattern(pattern);
			try {
				return SDF.parse(dateStr);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * 将指定日期字符串格式化成另一种格式的日期字符串
	 * 
	 * @param srcDate
	 *            源日期字符串
	 * @param fromFormat
	 *            原日期字符串格式
	 * @param toFormat
	 *            目标日期字符串格式
	 * @return 返回转换后的日期字符串
	 */
	public static String convert(String srcDate, String fromFormat, String toFormat) {
		return format(parse(srcDate, fromFormat), toFormat);
	}
}
