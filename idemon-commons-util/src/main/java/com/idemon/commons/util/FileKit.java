package com.idemon.commons.util;

import java.io.File;
import java.io.IOException;

/**
 * 
 * <p>
 * Title: FileKit
 * </p>
 * <p>
 * Description: 通用文件工具类
 * </p>
 * 
 * @author idemon
 * @date 2016年8月25日 上午10:50:05
 */
public class FileKit {
	/**
	 * 以当前时间毫秒作为文件名称
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getNewFileName(String fileName) {
		StringBuilder builder = new StringBuilder(fileName);
		builder.replace(0, builder.lastIndexOf("."), DateKit.getCurrMS() + "");
		return builder.toString();
	}

	/**
	 * 获取文件名称的前缀（即去除文件类型后缀）
	 * 
	 * @param fileName
	 *            文件全名
	 * @return 文件名称
	 */
	public static String getFilePrefix(String fileName) {
		String prefix = null;

		int dot = fileName.lastIndexOf(".");
		if (dot == -1) {
			prefix = fileName;
		} else {
			prefix = fileName.substring(0, dot);
		}
		return prefix;
	}

	/**
	 * 获取文件类型后缀
	 * 
	 * @param fileName
	 *            文件全名
	 * @return 文件后缀（.xxx）
	 */
	public static String getFileExt(String fileName) {
		String ext = null;
		int dot = fileName.lastIndexOf(".");
		if (dot == -1) {
			ext = "";
		} else {
			ext = fileName.substring(dot); // includes "."
		}
		return ext;
	}

	/**
	 * 重命名文件，保证不重名
	 * 
	 * @param parentPath
	 * @param originalFileName
	 * @return
	 */
	public static synchronized File rename(String parentPath, String originalFileName) {
		File destPath = new File(parentPath);
		if (!destPath.exists()) {
			if (!destPath.mkdirs()) {
				throw new RuntimeException("Can't create the directory " + parentPath);
			}
		}

		File f = null;
		f = new File(destPath, originalFileName);
		if (createNewFile(f)) {
			return f;
		}

		String prefix = getFilePrefix(originalFileName);
		String ext = getFileExt(originalFileName);

		int count = 0;
		do {
			f = new File(destPath, new StringBuffer(prefix).append("_").append(10000 + count++).append(ext).toString());
		} while (!createNewFile(f));

		return f;
	}

	/**
	 * 创建文件
	 * 
	 * @param f
	 * @return
	 */
	public static boolean createNewFile(File f) {
		try {
			return f.createNewFile();
		} catch (IOException ignored) {
			return false;
		}
	}

	/**
	 * 创建隐藏文件
	 * 
	 * @param dir
	 * @param fileName
	 * @return
	 */
	public static File createHiddenFile(String dir, String fileName) {
		File tempFile = rename(dir, fileName);
		// 设置文件和文件夹为隐藏
		try {
			Runtime.getRuntime().exec("attrib " + "\"" + tempFile.getAbsolutePath() + "\"" + " +H -R");
		} catch (IOException e) {
			Kit.showDebugInfo(e.getMessage());
		}
		return tempFile;
	}
}
