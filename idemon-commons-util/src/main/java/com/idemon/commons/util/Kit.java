package com.idemon.commons.util;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * <p>
 * Title: Kit
 * </p>
 * <p>
 * Description: 通用工具类
 * </p>
 * 
 * @author idemon
 * @date 2016年8月25日 上午10:49:29
 */
public class Kit {
	/**
	 * 数组去重复并排序
	 * 
	 * @param arr
	 *            可能有重复数据的数组
	 * @return 去重复并排序之后的数组
	 */
	public static String[] deduplicate(String... arr) {
		Set<String> set = new HashSet<String>();
		set.addAll(Arrays.asList(arr));
		String[] tmp = set.toArray(new String[0]);
		Arrays.sort(tmp);
		return tmp;
	}

	/**
	 * 判断数值是否在指定范围之内（包含临界值）
	 * 
	 * @param value
	 * @param begin
	 * @param end
	 * @return
	 */
	public static boolean isBetween(int value, int begin, int end) {
		if (value >= begin && value <= end) {
			return true;
		}
		return false;
	}

	/**
	 * 将List中的字符串中的“*”替换成“[a-zA-Z0-9./\\-_]*”后返回
	 * 
	 * @param patterns
	 * @return
	 */
	public static List<String> convertToPatterns(List<String> patterns) {
		if (patterns == null || patterns.isEmpty()) {
			return null;
		}
		for (int i = 0; i < patterns.size(); i++) {
			patterns.set(i, patterns.get(i).replace("*", "[a-zA-Z0-9\\-_]*"));
		}
		return patterns;
	}

	/**
	 * 判断字符串是否匹配正则表达式列表
	 * 
	 * @param patterns
	 *            正则表达式列表
	 * @param val
	 *            指定字符串
	 * @return
	 */
	public static boolean isIncluded(List<String> patterns, String val) {
		if (patterns == null || patterns.isEmpty()) {
			return false;
		}
		for (int i = 0; i < patterns.size(); i++) {
			if (Pattern.matches(patterns.get(i), val)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断字符串是否匹配正则表达式数组
	 * 
	 * @param patterns
	 *            正则表达式数组
	 * @param val
	 *            指定字符串
	 * @return true：包含；false：不包含
	 */
	public static boolean isIncluded(String[] patterns, String val) {
		if (ArrayUtils.isNotEmpty(patterns)) {
			for (int i = 0; i < patterns.length; i++) {
				if (Pattern.matches(patterns[i], val)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 判断指定字符串中是否包含数组中的字符（忽略大小写）
	 * 
	 * @param patterns
	 *            数组字符，如果为空，则返回-1
	 * @param val
	 *            指定字符串,如果为空，则返回-1
	 * @return 如果指定字符串中是否包含数组中的字符，则返回该字符在数组中的索引；否则返回-1；
	 */
	public static int isIncludedIgnoreCase(String[] patterns, String val) {
		if (StringUtils.isNotBlank(val) && ArrayUtils.isNotEmpty(patterns)) {
			if (ArrayUtils.isNotEmpty(patterns)) {
				for (int i = 0; i < patterns.length; i++) {
					if (val.toLowerCase().indexOf(patterns[i].toLowerCase()) > -1) {
						return i;
					}
				}
			}
		}
		return -1;
	}

	/**
	 * 获取是否处于DEBUG模式（Constants.
	 * IDEMON_CONFIG_FILENAME指定的文件中的isdebug值优先级高于Constants.IS_DEBUG）
	 * 
	 * @return
	 */
	public static boolean isDebug() {
		return PropKit.use(Constants.IDEMON_CONFIG_FILENAME).getBoolean("isdebug", Constants.IS_DEBUG);
	}

	/**
	 * 获取日志头（格式：[2015-05-05 12:34:23 230] [com.idemon.commons.util.Kit]）
	 * 
	 * @param obj
	 * @return
	 */
	public static StringBuilder generalLogHead(Object obj) {
		StringBuilder builder = new StringBuilder("[");
		builder.append(DateKit.formatNow(DateKit.NYRSFMH1));
		builder.append("] [");
		builder.append(obj.getClass().getName());
		builder.append("] ");
		return builder;
	}

	/**
	 * 打印信息<br/>
	 * （<br/>
	 * 格式：<br/>
	 * [2015-05-05 12:34:23 230] INFO:
	 * com.idemon.commons.util.Kit.method():[====info====]@line330<br/>
	 * ）<br/>
	 * 
	 * @param info
	 */
	public static void showDebugInfo(String info) {
		StackTraceElement stack[] = Thread.currentThread().getStackTrace();
		StringBuffer buffer = new StringBuffer("[");
		buffer.append(DateKit.formatNow(DateKit.NYRSFMH1));
		buffer.append("] INFO:");
		buffer.append(stack[2].getClassName());
		buffer.append(".");
		buffer.append(stack[2].getMethodName());
		buffer.append("():");
		buffer.append("[====");
		buffer.append(info);
		buffer.append("====]");
		buffer.append("@line:");
		buffer.append(stack[2].getLineNumber());
		System.out.println(buffer.toString());
	}

	/**
	 * 如果value.length大于等于length，那么原样输出，否则加前导0以补足位数<br/>
	 * 例如："23"长度为2，指定补足长度为5，那么输出"00023"
	 * 
	 * @param value
	 *            整数格式字符串，例如："23423"
	 * @param length
	 *            指定补足总长度
	 * @return
	 */
	public static String formatNO(String value, int length) {
		float ver = Float.parseFloat(System.getProperty( // 获取JDK的版本
				"java.specification.version"));
		String laststr = "";
		if (ver < 1.5) { // JDK1.5以下版本执行的语句
			try {
				NumberFormat formater = NumberFormat.getNumberInstance();
				formater.setMinimumIntegerDigits(length);
				laststr = formater.format(value).toString().replace(",", "");
			} catch (Exception e) {
				System.out.println("格式化字符串时的错误信息：" + e.getMessage()); // 输出异常信息
			}
		} else { // JDK1.5版本执行的语句
			Integer[] arr = new Integer[1];
			arr[0] = new Integer(value);
			laststr = String.format("%0" + length + "d", arr);
		}
		return laststr;
	}
}
