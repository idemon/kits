package com.idemon.commons.util;

import java.io.UnsupportedEncodingException;

/**
 * 
 * <p>
 * Title: StringKit
 * </p>
 * <p>
 * Description: 字符串工具类
 * </p>
 * 
 * @author idemon
 * @date 2016年8月25日 上午10:59:00
 */
public class StringKit {
	/**
	 * 为字符串转码
	 * 
	 * @param srcStr
	 *            源字符串
	 * @param fromEncoding
	 *            源编码
	 * @param toEncoding
	 *            目标编码
	 * @return 转码之后字符串
	 */
	public static String convertEncoding(String srcStr, String fromEncoding, String toEncoding) {
		if (srcStr == null || srcStr.isEmpty()) {
			return srcStr;
		} else {
			try {
				return new String(srcStr.getBytes(fromEncoding), toEncoding);
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * 将ISO-8859-1编码的字段串转换为UTF-8编码
	 * 
	 * @param srcStr
	 * @return
	 */
	public static String iso2utf8(String srcStr) {
		return convertEncoding(srcStr, Constants.ENCODING_ISO, Constants.ENCODING_UTF8);
	}

	/**
	 * 对输入的字符串进行一次编码转换，防止SQL注入
	 * 
	 * @param sql
	 * @return
	 */
	public static String convertSQL(String sql) {
		if (sql == null || sql.isEmpty()) {
			return ""; // 返回空的字符串
		} else {
			try {
				sql = sql.trim().replace('\'', (char) 32); // 将'号转换化为空格
			} catch (Exception e) {
				return "";
			}
		}
		return sql;
	}
	/**
	 * 合并字符串
	 * @param str
	 * @return
	 */
	public static String merge(Object... str) {
		StringBuilder builder = new StringBuilder(0);
		for (Object tmp : str) {
			builder.append(tmp);
		}
		return builder.toString();
	}
}
