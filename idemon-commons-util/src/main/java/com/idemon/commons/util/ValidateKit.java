package com.idemon.commons.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ValidateKit {
	private static final String PROP_FILE_NAME = "idemon-validator.properties";
	private static Map<String, Pattern> patterns = new HashMap<>(0);
	private static Prop prop;

	/**
	 * 通过java正则表达式验证字符串有效性，正则式在idemon-validator.properties文件中配置
	 * 
	 * @param value	需要校验的字符串
	 * @param key	校验类型（即在idemon-validator.properties中配置的正则表达式的key）
	 * @return
	 */
	public static boolean validate(String value, String key) {
		return getPattern(key).matcher(value).matches();
	}
	/**
	 * 通过指定java正则表达式验证字符串有效性
	 * @param value	需要校验的字符串
	 * @param regex	指定java正则表达式
	 * @return
	 */
	public static boolean validateByRegex(String value, String regex) {
		return Pattern.compile(regex.trim()).matcher(value).matches();
	}

	private static Pattern getPattern(String key) {
		if (prop == null) {
			prop = PropKit.use(PROP_FILE_NAME);
		}

		if (patterns.containsKey(key)) {
			return patterns.get(key);
		} else {
			if (prop == null) {
				throw new RuntimeException("idemon-validator.properties文件不存在");
			}
			String patt = prop.get(key);
			if (patt == null || patt.isEmpty()) {
				throw new RuntimeException("idemon-validator.properties文件中找不到key[" + key + "]");
			}
			Pattern pattern = Pattern.compile(patt.trim());
			patterns.put(key, pattern);
			return pattern;
		}
	}
}
