package com.idemon.plugin.encoding.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
/**
 * 
 * <p>Title: CharacterEncodingFilter</p>
 * <p>Description: 当使用某些框架时，自动过滤可能失效，需要手动配置本过滤器，放置在其他过滤器之前</p>
 * 
 * @author idemon
 * @date 2016年8月25日 上午11:06:11
 */
public class CharacterEncodingFilter implements Filter {

	private String encoding = "UTF-8";

	public void destroy() {
		encoding = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding(encoding);
		chain.doFilter(request, response);
		response.setCharacterEncoding(encoding);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		String code = fConfig.getInitParameter("encoding");
		if(code != null && !code.isEmpty()){
			encoding = code;
		}
	}
}
