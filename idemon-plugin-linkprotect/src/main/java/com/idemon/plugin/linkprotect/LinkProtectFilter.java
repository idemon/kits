package com.idemon.plugin.linkprotect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idemon.commons.util.Kit;

public class LinkProtectFilter implements Filter {

	private static final String REFERER = "referer";
	private static final String ERROR_PAGE_URL = "errorPageURL";
	private static final String WHITE_LIST = "whiteList";
	private static final String SEP = ",";

	private String errorPageURL;
	private String basePath;

	private List<String> whiteList = new ArrayList<>(0);

	public void destroy() {
		errorPageURL = null;
		basePath = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		if (StringUtils.isBlank(basePath)) {
			basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ req.getContextPath() + "/";
		}
		String referer = req.getHeader(REFERER);
		if (null == referer || !referer.trim().startsWith(basePath)
				|| (!Kit.isIncluded(whiteList, req.getRequestURI()))) {
			// 盗链
			req.getRequestDispatcher(errorPageURL).forward(req, res);
		} else {
			chain.doFilter(req, res);
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
		errorPageURL = fConfig.getInitParameter(ERROR_PAGE_URL);
		if (StringUtils.isBlank(errorPageURL)) {
			throw new RuntimeException(getClass().getName() + "在web.xml中未配置初始化参数：" + ERROR_PAGE_URL);
		}
		String whiteListStr = fConfig.getInitParameter(WHITE_LIST);
		if (StringUtils.isNotBlank(whiteListStr)) {
			whiteList = Arrays.asList(whiteListStr.split(SEP));
			whiteList = Kit.convertToPatterns(whiteList);
		}
	}

}
