package com.idemon.plugin.login;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.idemon.commons.util.Constants;
import com.idemon.commons.util.Kit;

/**
 * Servlet Filter implementation class LoginFilter
 */
public class LoginFilter implements Filter {

	private String[] includes;
	private String[] excludes;
	private String loginURL;
	private String[] sessionKeys;
	private String[] suffixs;

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		includes = null;
		excludes = null;
		loginURL = null;
		sessionKeys = null;
		suffixs = null;
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		boolean isGreenLight = false;
		HttpServletRequest req = ((HttpServletRequest) request);
		String uri = req.getRequestURI();
		System.out.println("===>" + uri);
		int i = uri.indexOf(".", uri.lastIndexOf("/"));
		if (i != -1 && Arrays.binarySearch(suffixs, uri.substring(i)) >= 0) {
			isGreenLight = true;
		} else if (ArrayUtils.isNotEmpty(excludes) && Kit.isIncluded(excludes, uri)) {
			isGreenLight = true;
		} else if (ArrayUtils.isNotEmpty(includes) && Kit.isIncluded(includes, uri)) {
			// 属于禁行访问路径
			HttpSession session = req.getSession(false);
			if (session != null) {
				for (String tmp : sessionKeys) {
					if (session.getAttribute(tmp) != null) {
						isGreenLight = true;
						break;
					}
				}
			}
		} else {
			isGreenLight = true;
		}
		if (isGreenLight) {
			chain.doFilter(request, response);
		} else {
			((HttpServletResponse) response).sendRedirect(req.getContextPath() + loginURL);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		String sessionKeyStr = fConfig.getInitParameter(ProxyServletContextListener.INIT_PARAM_USER_SESSION_KEY);
		if (StringUtils.isBlank(sessionKeyStr)) {
			throw new ServletException(Kit.generalLogHead(this).append("请在").append(Constants.IDEMON_CONFIG_FILENAME)
					.append("文件中配置").append(ProxyServletContextListener.KEY_LOGIN_USER_SESSION_KEY).toString());
		} else {
			sessionKeys = sessionKeyStr.split(",");
		}
		loginURL = fConfig.getInitParameter(ProxyServletContextListener.INIT_PARAM_LOGIN_URL);
		if (StringUtils.isBlank(loginURL)) {
			throw new ServletException(Kit.generalLogHead(this).append("请在").append(Constants.IDEMON_CONFIG_FILENAME)
					.append("文件中配置").append(ProxyServletContextListener.KEY_LOGIN_URL).toString());
		}
		if (!loginURL.startsWith("/")) {
			throw new ServletException(Kit.generalLogHead(this).append(Constants.IDEMON_CONFIG_FILENAME).append("文件中")
					.append(ProxyServletContextListener.KEY_LOGIN_URL).append("必须以'/'开头").toString());
		}
		String includesStr = fConfig.getInitParameter(ProxyServletContextListener.INIT_PARAM_INCLUDES);
		if (StringUtils.isNotBlank(includesStr)) {
			includes = includesStr.split(",");
			convertToPatterns(includes);
		}
		String excludesStr = fConfig.getInitParameter(ProxyServletContextListener.INIT_PARAM_EXCLUDES);
		if (StringUtils.isNotBlank(excludesStr)) {
			excludes = excludesStr.split(",");
			convertToPatterns(excludes);
		}
		String staticResSuffixStr = fConfig.getInitParameter(ProxyServletContextListener.INIT_PARAM_STATIC_RES_SUFFIX);
		if (StringUtils.isNotBlank(staticResSuffixStr)) {
			suffixs = staticResSuffixStr.split(",");
			Arrays.sort(suffixs);
		}
	}

	private void convertToPatterns(String[] patterns) {
		for (int i = 0; i < patterns.length; i++) {
			patterns[i] = patterns[i].replace("*", "[a-zA-Z0-9\\-_]*");
		}
	}
}
