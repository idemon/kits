package com.idemon.plugin.login;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.idemon.commons.util.Constants;
import com.idemon.commons.util.Prop;
import com.idemon.commons.util.PropKit;

/**
 * excludes优先级高于includes
 * 
 * @author idemon
 * @date 2016年5月30日
 */
public class ProxyServletContextListener implements ServletContextListener {

	public static final String INIT_PARAM_INCLUDES = "includes";
	public static final String INIT_PARAM_EXCLUDES = "excludes";
	public static final String INIT_PARAM_LOGIN_URL = "login_url";
	public static final String INIT_PARAM_USER_SESSION_KEY = "user_session_key";
	public static final String INIT_PARAM_STATIC_RES_SUFFIX = "static_res_suffix";
	public static final String KEY_LOGIN_FILTER_INCLUDES = "login.filter.red.light";
	public static final String KEY_LOGIN_FILTER_EXCLUDES = "login.filter.green.light";
	public static final String KEY_LOGIN_FILTER_URL_PATTERNS = "login.filter.url.patterns";
	public static final String KEY_LOGIN_STATIC_RES_SUFFIX = "login.static.res.suffix";
	public static final String KEY_LOGIN_URL = "login.url";
	public static final String KEY_LOGIN_USER_SESSION_KEY = "login.user.session.key";

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		PropKit.useless(Constants.IDEMON_CONFIG_FILENAME);
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		FilterRegistration.Dynamic d1 = arg0.getServletContext().addFilter(LoginFilter.class.getName(),
				LoginFilter.class);
		Prop prop = PropKit.use(Constants.IDEMON_CONFIG_FILENAME);
		d1.setInitParameter(INIT_PARAM_INCLUDES, prop.get(KEY_LOGIN_FILTER_INCLUDES));
		d1.setInitParameter(INIT_PARAM_EXCLUDES, prop.get(KEY_LOGIN_FILTER_EXCLUDES));
		d1.setInitParameter(INIT_PARAM_LOGIN_URL, prop.get(KEY_LOGIN_URL));
		d1.setInitParameter(INIT_PARAM_USER_SESSION_KEY, prop.get(KEY_LOGIN_USER_SESSION_KEY));
		d1.setInitParameter(INIT_PARAM_STATIC_RES_SUFFIX, prop.get(KEY_LOGIN_STATIC_RES_SUFFIX));
		EnumSet<DispatcherType> dispatcherTypes = EnumSet.allOf(DispatcherType.class);
		dispatcherTypes.add(DispatcherType.REQUEST);
		dispatcherTypes.add(DispatcherType.FORWARD);
		dispatcherTypes.add(DispatcherType.INCLUDE);
		dispatcherTypes.add(DispatcherType.ASYNC);
		d1.addMappingForUrlPatterns(dispatcherTypes, true, prop.get(KEY_LOGIN_FILTER_URL_PATTERNS));
	}

}
